# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

PROMPT_COMMAND='history -a'

shopt -s histappend
shopt -s cdspell
shopt -s dotglob
shopt -s checkwinsize
shopt -s cmdhist

export IGNOREEOF=1
export CLICOLOR=1
export LSCOLORS="ExGxcxdxbxegedabagacad"
export EDITOR=nano
export HISTSIZE=10000
export SAVEHIST=10000
export HISTIGNORE="&:ls:ll:la:l.:pwd:exit:clear:clr"

has_virtualenv() {
    if [ -e .venv ]; then
        CURRENT_VENV=$WORKON_HOME/`cat .venv`
        if [ "$CURRENT_VENV" != "$VIRTUAL_ENV" ]; then
            workon `cat .venv`
        fi
    fi
}

venv_cd () {
    cd "$@" && has_virtualenv
}

# Aliases
alias cd="venv_cd"
alias ..="cd .."
alias ...="cd ..."
alias n="nano"
alias du='du -csh'
alias df='df -h'
alias g="git"
alias top="htop"
alias ll="ls -lha"
alias psa="ps auxf"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias cleanunder="find ./ -type f -name '._*' -exec rm -f {} \;"

# If pyenv
# export PATH="/home/uninen/.pyenv/bin:$PATH"
# eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"

# Virtualenvwrapper
# export WORKON_HOME=$HOME/.virtualenvs
# export PROJECT_HOME=$HOME/projects
#pyenv virtualenvwrapper
