# My Dotfiles

Opinnionated, tweaked for Ubuntu 18.04, not fancy.

## Howto

`git clone https://gitlab.com/uninen/dotfiles dotfiles`

`./dotfiles/bootstrap.sh`

## Tools

- bash (on Ubuntu, zsh on MacOS)
- pyenv
- virtualenvwrapper (via pyenv-virtualenvwrapper)
