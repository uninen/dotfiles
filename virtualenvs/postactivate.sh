#!/bin/bash
# This hook is sourced after every virtualenv is activated.

export PROJECT_NAME=$(echo $VIRTUAL_ENV|awk -F'/' '{print $NF}')
export GIT_PROJECT_ROOT="$HOME/projects/$PROJECT_NAME"

# General
alias cdv="cd $GIT_PROJECT_ROOT"
alias cds="cdsitepackages"

# Django
alias f="fab -f $GIT_PROJECT_ROOT/conf/fabfile.py"
alias m="python \"$GIT_PROJECT_ROOT/$PROJECT_NAME/manage.py\""
alias run="m runserver 0.0.0.0:8000"

export PYTHONPATH=$PATH

if [ -f "$GIT_PROJECT_ROOT/.env" ] ; then
    export $(cat $GIT_PROJECT_ROOT/.env)
fi

if [ -d "$GIT_PROJECT_ROOT" ] ; then
    echo " "
    echo "Welcome to $PROJECT_NAME :)"
    cd ~/projects/$PROJECT_NAME
fi
