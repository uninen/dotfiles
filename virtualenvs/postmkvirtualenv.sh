#!/bin/bash
# This hook is run after a new virtualenv is activated.

export PROJECT_NAME=$(echo $VIRTUAL_ENV|awk -F'/' '{print $NF}')
export GIT_PROJECT_ROOT="$HOME/projects/$PROJECT_NAME"

if [ ! -d "$GIT_PROJECT_ROOT" ] ; then
    mkdir $GIT_PROJECT_ROOT
    cd $GIT_PROJECT_ROOT
    echo $PROJECT_NAME > .venv
    echo " "
    echo "Created project directory. Welcome to $PROJECT_NAME :)"
fi
